import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    aboutText: {
        padding: "65px"
    },
    header: {
        color: "purple"
    },
    footer: {
        fontWeight: "bold",
        textAlign: "center",
        paddingTop: "50px"
    }
}
)
)
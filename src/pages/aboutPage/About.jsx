import React from 'react'
import useStyles from './styles.js';

const About = () => {
    const classes = useStyles();
    return (
        <div className={classes.aboutText}>
            <div>
            <h2 className={classes.header}>About LPA</h2>
            </div>
            <div>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos, ipsum eius saepe iste dolorum corrupti ullam quibusdam culpa molestiae quasi provident dolores in similique quas distinctio. Quibusdam dolore quam harum.</p>
            </div>
            <div>
                <footer className={classes.footer}>© 2021 Accenture. All Rights Reserved</footer>
            </div>
        </div>
        
    );
}

export default About

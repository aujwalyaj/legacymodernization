import { Typography, Grid, Card, CardContent, Button } from '@material-ui/core';
import React, { Fragment } from 'react';
import useStyles from './styles.js';
import uploadImage from '../../../images/upload.png';
import scanningImage from '../../../images/scan.png';
import reportsImage from '../../../images/reports.png';
import { Link } from 'react-router-dom';


const CodeHome = ({setActiveLink}) => {
    const classes = useStyles();

  return (
    <>
        <div className={classes.heading}>
            <Typography variant='h3' gutterBottom className={classes.heading3}>Analyze Your Code.</Typography>
            <Typography variant='h4' gutterBottom className={classes.heading3}>In 3 simple steps.</Typography>
        </div>

        <Grid container 
        
        direction="column"
        alignItems="center"
        justifyContent="center">
            <Grid  className={classes.gridItem}>
                <Card className={classes.cardItem}>
            
                    <CardContent>
                        <div >
                            <div className={classes.imageDiv}>
                                <img src={uploadImage} alt="Upload Your Code..." className={classes.imageClass}/>
                            </div>
                            <Typography variant="h6" gutterbutton="true" className={classes.text}>
                                Upload Your Code
                            </Typography>
                            <Typography variant="h6" gutterBottom="true" className={classes.stepText}>
                                Step 1
                            </Typography>
                        </div>
                    </CardContent>        
                </Card> 
                {/* <Card className={classes.arrowCard}>
                    <img src={arrowImage} className={classes.arrowImage}/>
                </Card> */}
                <Card className={classes.cardItem}>
            
                    <CardContent>
                        <div >
                        <div className={classes.imageDiv}>
                                <img src={scanningImage} alt="Scan Your Code..." className={classes.imageClass}/>
                            </div>
                            <Typography variant="h6" gutterbutton="true" className={classes.text}>
                                Scan Your Code
                            </Typography>
                            <Typography variant="h6" gutterBottom="true" className={classes.stepText}>
                                Step 2
                            </Typography>
                        </div>
                    </CardContent>        
                </Card>
                 
                <Card className={classes.cardItem}>
            
                    <CardContent>
                        <div >
                            <div className={classes.imageDiv}>
                                <img src={reportsImage} alt="View Generated Report..." className={classes.imageClass}/>
                            </div>
                            <Typography variant="h6" gutterbutton="true" className={classes.text}>
                                View Analysis Report
                            </Typography>
                            <Typography variant="h6" gutterBottom="true" className={classes.stepText}>
                                Step 3
                            </Typography>
                        </div>
                    </CardContent>        
                </Card> 
                
            </Grid>
        </Grid>

        <div className={classes.getStartedDiv}>
            <Button variant="contained" component={Link} to="/upload" color="primary" className={classes.button} onClick={()=>setActiveLink("Upload Your Code")}>Get Started</Button>
        </div>
        
    </>
  );
};

export default CodeHome;

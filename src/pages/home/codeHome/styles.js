import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    heading: {
        marginTop: '50px',
        marginLeft: '70px'
    },
    heading3: {
        fontWeight: 'bold',
        color: 'purple'
    },

    imageClass: {
        zoom: '30%',
        marginTop: '50px',
        justifyContent: 'center'
    },
    gifClass: {
        zoom: '12%',
        marginTop: '50px',
        justifyContent: 'center'
    },
    gridItem: {
        display: "flex", 
        alignItems: "center",
        //paddingTop: "20px"
    },
    cardItem: {
        margin: "20px",
        boxShadow: "5px 5px 10px thistle",
        borderRadius: "10px",
        width: "250px"
    },
    imageDiv: {
        display: "flex",
        justifyContent: "center",
        marginBottom: "10px"
    },
    text: {
        display: "flex",
        justifyContent: "center",
        color: "purple",
        fontWeight: "600"
    },
    getStartedDiv: {
        display: "flex",
        justifyContent: "center",
        marginTop: "10px"
    },
    button: {
        backgroundColor: "purple",
        color: "white",
        fontWeight: "600"
    },
    arrowImage: {
        height: "100%",
        width: "100%"
    },
    arrowCard: {
        boxShadow: "none",
        width: "5rem",
        height: "5rem"
    },
    stepText: {
        display: "flex",
        justifyContent: "center",
        color: "#818589",
        fontWeight: "600"
    }

}
)
)
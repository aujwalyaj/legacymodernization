import React from 'react';
import Carousel from 'react-material-ui-carousel'
import CodeHome from './codeHome/CodeHome'
import DBHome from './dbHome/DBHome';
import {Paper} from '@material-ui/core'
import useStyles from './styles.js'

const LandingPage = ({setActiveLink}) => {
    const classes = useStyles();
    return (
        // <Paper className={classes.paperStyle}>
            <Carousel
                className={classes.carouselStyle}
                next={ (next, active) => console.log(`we left ${active}, and are now at ${next}`) }
                prev={ (prev, active) => console.log(`we left ${active}, and are now at ${prev}`) }
            >
                <CodeHome setActiveLink={setActiveLink}/>
                <DBHome  setActiveLink={setActiveLink}/>
                
            </Carousel>
        
        
    )
}


export default LandingPage
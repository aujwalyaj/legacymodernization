import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    paperStyle: {
        padding: "20px",
        // height: "70vh",
        width: "300px",
        marginTop: "140px",
        margin: "auto"
    },
    avatarStyle: {
        backgroundColor: "purple"
    },
    textBox: {
        margin: "10px 0px",
        color: "black"
    },
    submitButton: {
        backgroundColor: "purple",
        color: "white",
        fontWeight:"600",
        margin: "8px 0"
    }
}
)
)
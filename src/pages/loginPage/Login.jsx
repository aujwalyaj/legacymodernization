import { Avatar, Checkbox, FormControlLabel, Grid, Paper, TextField, Button, Typography, Link} from '@material-ui/core'
import React, {useState} from 'react'
import { useHistory } from "react-router-dom";
import useStyles from './style.js';
import { LockOutlined, AddCircleOutline } from '@material-ui/icons';

const Login = ({setActiveLink}) => {
    const classes = useStyles();
    let history = useHistory();
    const [newUser,setNewUser] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    // const usernameRef = React.useRef();
    // const passwordRef = React.useRef();

    const handleSubmit = e => {
        // e.preventDefault();
        const data = {
            "username": username,
            "password": password
        };
        const json = JSON.stringify(data, null, 4);
        console.clear();
        console.log(json);
        if (newUser) {
            alert("Your Account is Created.")
            setNewUser(!newUser);
            setUsername('');
            setPassword('');
        } else {
            setActiveLink("Upload Your Code")
            history.push('/upload')
        }
    };

    return (
        <Grid>
            <Paper elevation={10} className={classes.paperStyle}>
               <Grid align="center">
                    <Avatar className={classes.avatarStyle}>{(newUser) ? <AddCircleOutline/> : <LockOutlined/>}</Avatar>
                    <h2>{(newUser) ? 'Sign Up' : 'Sign In'}</h2>
                </Grid>
                <TextField 
                    value={username} 
                    onChange={(e) => setUsername(e.target.value)}
                    className={classes.textBox} label="Username" placeholder="Enter Username" fullWidth required></TextField>
                <TextField 
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                    className={classes.textBox} label="Password" placeholder="Enter Password" type="password" fullWidth required></TextField>
                {/* <FormControlLabel
                    control={
                        <Checkbox
                        name="checkedB"
                        color="primary"
                        />
                    }
                    label="Remember Me"
                />   */}
                <Button type="submit" onClick={handleSubmit} className={classes.submitButton} variant="contained" fullWidth>{(newUser) ? 'Sign Up' : 'Sign In'}</Button> 
                <Typography align="center">  {(newUser) ? 'Already have an account?' : 'Do you have an account? '}
                    <br/>
                    <Link onClick={()=>{setNewUser(!newUser)}}  style={{ textDecoration: 'none', cursor: 'pointer'}}>
                        {(newUser) ? 'Sign In' : 'Sign Up'}
                    </Link>
                </Typography>       
            </Paper>
        </Grid>
    )
}

export default Login

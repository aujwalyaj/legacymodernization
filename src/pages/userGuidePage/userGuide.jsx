import React from 'react'

import { PDFViewer } from 'react-view-pdf';

import pdfURL from '../../assets/sample.pdf'

const UserGuide = () => {
    return (
        // <PDFViewer url="http://www.africau.edu/images/default/sample.pdf" />
        <PDFViewer url={pdfURL} />

        // <PDFViewer
        //     file={{
        //         url: 'http://www.africau.edu/images/default/sample.pdf'
        //     }}
        // />
    );
}

export default UserGuide

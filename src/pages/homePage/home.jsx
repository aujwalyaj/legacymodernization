import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(6, 4),
  },
  card: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
    boxShadow: '5px 5px 10px #888888',
    borderRadius: '25px',
    textAlign: 'left',
  },
  title: {
    padding: theme.spacing(2),
    fontWeight: 'bold',
  },
  featureList: {
    padding: theme.spacing(2),
    color: '#808080',
  },
  bottomCard: {
    height: '150%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
    boxShadow: '5px 5px 10px #888888',
    borderRadius: '25px',
    textAlign: 'left',
  },
}));

export default function OutlinedCard() {
  const classes = useStyles();

  return (
    <Container component="section" maxWidth="lg" className={classes.root}>
      <Grid container spacing={3} alignItems="stretch">
        <Grid item xs={12} sm={4}>
          <div className={classes.card}>
            <Typography variant="h6" component="h3" className={classes.title}>
              Services
            </Typography>
            <Typography variant="h4" className={classes.title}>
              5
            </Typography>
            <Typography className={classes.featureList}>
              Overall number of Services
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} sm={4}>
          <div className={classes.card}>
            <Typography variant="h6" component="h3" className={classes.title}>
              Classes
            </Typography>
            <Typography variant="h4" className={classes.title}>
              21
            </Typography>
            <Typography className={classes.featureList}>
            Overall number of Classes
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} sm={4}>
          <div className={classes.card}>
            <Typography variant="h6" component="h3" className={classes.title}>
            Interfaces
            </Typography>
            <Typography variant="h4" className={classes.title}>
              11
            </Typography>
            <Typography className={classes.featureList}>
            Overall number of Interfaces
            </Typography>
          </div>
        </Grid>
      </Grid>
      <br/>
      <br/>
      <br/>
      <Grid container spacing={3} alignItems="stretch">
        <Grid item lg={12}>
          <div className={classes.bottomCard}>
            <Typography variant="h6" component="h3" className={classes.title}>
              Service Analytics (Radar Chart)
            </Typography>
            <Typography variant="h4" className={classes.title}>
              
            </Typography>
            <Typography className={classes.featureList}>
              
            </Typography>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}

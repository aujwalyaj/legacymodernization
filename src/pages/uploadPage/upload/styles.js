import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    submitBtnColor: {
        backgroundColor: '#800080 !important',
        borderColor: '#800080 !important',
        color: 'white',
        borderRadius: '5px',
        height: '40px',
        border: 'none',
        marginTop: '15px'
    },
    uploadForm: {
        width: 'fit-content',
        marginTop: '160px',
        zoom: '125%'
    },
    uploadFormDiv: {
        // marginLeft: 'auto',
        // marginRight: 'auto',
        // marginTop: '100px',
        width: 'fit-content'
    },
    uploadButton: {
        marginLeft: '85px',
        marginBottom: '15px'
    },
    imageClass: {
        zoom: '25%',
        marginTop: '150px'
    },

    box: {
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        flexWrap: 'wrap',
        '& > *': {
            marginTop: theme.spacing(3),
            width: theme.spacing(38),
            height: theme.spacing(6),
            borderRadius: theme.spacing(1),
            backgroundColor: '#EFE8F3 !important'
        },

    },

    fileList: {
        padding: '5px',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'baseline',
    },

    icon: {
        justifyContent: 'space-evenly',
        paddingLeft: '5px'
    },

    uploadCode: {
        display: 'flex',
        alignContent: 'baseline',
        justifyContent: 'center',
        flexDirection: 'column',
        flexWrap: 'wrap',
        marginTop: theme.spacing(10),
        padding: '5px',
        '& > *': {
            width: theme.spacing(35),
            height: theme.spacing(6),
            borderRadius: theme.spacing(1),
            backgroundColor: '#EFE8F3 !important'
        },
    },

    buttonAlignment: {
        display: 'flex',
        justifyContent: 'center'
    },

    pageNumbers: {
        listStyle: 'none',
        display: "flex",
        justifyContent: 'center',
        marginTop: theme.spacing(5),
        '& > *': {
            padding: '10px',
            border: '1px solid white',
            cursor: 'pointer',
            '& > *': {
                // backgroundColor: '#800080 !important',
                border: 'none',
                color: 'white',
                fontSize: '1.5rem',
                cursor: 'pointer',
            }
        }

    },
    disabledButton: {
        color:"#e0e0e0"
    },
    enabledButton: {
        // backgroundColor:"purple",
        color:"purple"
    }

}
)
)
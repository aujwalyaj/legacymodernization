import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import styles from './styles.js';
import FileList from "./FileList";

function UploadPageNew() {
    const classes = styles();
    const [file, setFile] = useState('');
    const [filename, setFilename] = useState('Choose File');

    const fileList = ['File1.zip', 'File2.zip', 'File3.war'];

    const onChange = e => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    };

    const onSubmit = async e => {
        e.preventDefault();
        console.log(e)
        alert('Code submitted')
    }

    return (
        <>
            <FileList />
            <div className={classes.box}>
                <Paper className={classes.uploadCode}>
                    <input
                        type='file'
                        id='customFile'
                        onChange={onChange}
                        className={classes.fileList}
                    />

                </Paper>
            </div>
            <div className={classes.buttonAlignment}>
                <input
                    type='submit'
                    value='Upload Code'
                    className={classes.submitBtnColor}
                    onClick={onSubmit}
                />
            </div>

        </>
    )
}

export default UploadPageNew

import React, { useState, useEffect } from 'react'
import DocumentScannerIcon from '@mui/icons-material/DocumentScanner';
import BarChartIcon from '@mui/icons-material/BarChart';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import styles from './styles.js';
import Paper from '@material-ui/core/Paper';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Button } from '@material-ui/core';
import StorageIcon from '@mui/icons-material/Storage'; 


function FileList() {

    const classes = styles();

    const filesList = [
        {
          "id": 1,
          "name": "File1.zip",
          "type": "Code"
        },{
            "id": 2,
            "name": "File2.zip",
            "type": "Code"
        },{
            "id": 3,
            "name": "DB1 Details",
            "type": "DB"
        }
    ]


    const [data, setData] = useState([]);


    const [currentPage, setcurrentPage] = useState(1);
    const [itemsPerPage, setitemsPerPage] = useState(3);

    const [pageNumberLimit, setpageNumberLimit] = useState(1);
    const [maxPageNumberLimit, setmaxPageNumberLimit] = useState(2);
    const [minPageNumberLimit, setminPageNumberLimit] = useState(0);

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);

    const pages = [];
    for (let i = 1; i <= Math.ceil(data.length / itemsPerPage); i++) {
        pages.push(i);
    }

 
    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((response) => response.json())
            .then((json) => setData(json));
    }, []);

    const handleNextbtn = () => {
        setcurrentPage(currentPage + 1);

        if (currentPage + 1 > maxPageNumberLimit) {
            setmaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
            setminPageNumberLimit(minPageNumberLimit + pageNumberLimit);
        }
    };

    const handlePrevbtn = () => {
        setcurrentPage(currentPage - 1);

        if ((currentPage - 1) % pageNumberLimit === 0) {
            setmaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
            setminPageNumberLimit(minPageNumberLimit - pageNumberLimit);
        }
    };

    let pageIncrementBtn = null;
    if (pages.length > maxPageNumberLimit) {
        pageIncrementBtn = <li onClick={handleNextbtn}> &hellip; </li>;
    }

    let pageDecrementBtn = null;
    if (minPageNumberLimit >= 1) {
        pageDecrementBtn = <li onClick={handlePrevbtn}> &hellip; </li>;
    }
    const handleClick = (event) => {
        setcurrentPage(Number(event.target.id));
    };

    const renderPageNumbers = pages.map((number) => {
        if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
            return (
                <li
                    key={number}
                    id={number}
                    onClick={handleClick}
                    className={currentPage === number ? "active" : null}
                >
                    {number}
                </li>
            );
        } else {
            return null;
        }
    });

    const renderData = (data) => {
        return filesList.map((file, index) => (
            <div className={classes.box}>
                <Paper
                    key={index} elevation={1} >
                    <div className={classes.fileList} >
                        <span>{file.name}</span>
                        <span className={classes.icon}>
                            {file.type === "DB"?
                                <Tooltip title="Refactor">
                                    <IconButton>
                                        <StorageIcon />
                                    </IconButton>
                                </Tooltip>
                            : 
                                <Tooltip title="Scan">
                                    <IconButton>
                                        <DocumentScannerIcon />
                                    </IconButton>
                                </Tooltip>
                            }
                            
                            <Tooltip title="Report">
                                <IconButton>
                                    <BarChartIcon />
                                </IconButton>
                            </Tooltip>
                        </span>

                    </div>
                </Paper>
            </div>

        ))

    }
    return (
        <>
            {renderData(currentItems)}
            <div className={classes.pageNumbers}>
                <Button
                    onClick={handlePrevbtn}
                    variant="text"
                    disabled={currentPage === pages[0] ? true : false}
                     >
                    <NavigateBeforeIcon className={currentPage === pages[0] ? classes.disabledButton : classes.enabledButton}
               />
                </Button>
                {/* {pageDecrementBtn} */}
                {renderPageNumbers}
                {/* {pageIncrementBtn} */}

                <Button
                    onClick={handleNextbtn}
                    variant="text"
                    disabled={currentPage === pages[pages.length - 1] ? true : false}
                    
                >
                    <NavigateNextIcon className={currentPage === pages[pages.length - 1] ? classes.disabledButton : classes.enabledButton}
               />
                </Button>
            </div>
        </>
    )
}

export default FileList

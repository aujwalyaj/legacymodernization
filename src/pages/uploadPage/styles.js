import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    submitBtnColor: {
        backgroundColor: '#800080 !important',
        borderColor: '#800080 !important',
        color: 'white',
        borderRadius: '5px',
        height: '40px',
        border: 'none',
        marginTop: '15px'
    },
    uploadForm: {
        width: 'fit-content',
        marginTop: '160px',
        zoom: '125%'
    },
    uploadFormDiv: {
        // marginLeft: 'auto',
        // marginRight: 'auto',
        // marginTop: '100px',
        width: 'fit-content'
    },
    uploadButton: {
        marginLeft: '85px',
        marginBottom: '15px'
    },
    imageClass: {
        zoom: '25%',
        marginTop: '150px'
    },
    confirmPopupOkBtn: {
        backgroundColor: 'purple !important'
    },
    submitBtnDisableColor: {
        backgroundColor: '#c9a0dc !important',
        borderColor: '#c9a0dc !important',
        color: 'white',
        borderRadius: '5px',
        height: '40px',
        border: 'none',
        marginTop: '15px'
    }

}
)
)
import React, { Fragment, useState } from 'react';
// import Message from './Message';
// import Progress from './Progress';
import useStyles from './styles.js';
import load6 from '../../images/load6.gif';
import tick7 from '../../images/tick7.png';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import './style.css'
import UploadPageNew from './upload/UploadPageNew.jsx';

const FileUpload = ({ setActiveLink }) => {

  const [isUploadPage, setIsUpload] = useState(true);
  const [isScanningCompleted, setScanningCompleted] = useState(false);
  const classes = useStyles();
  const [file, setFile] = useState('');
  const [filename, setFilename] = useState('Choose File');
  const [uploadedFile, setUploadedFile] = useState({});
  // const [message, setMessage] = useState('');
  const [uploadPercentage, setUploadPercentage] = useState(0);

  const onChange = e => {
    setFile(e.target.files[0]);
    setFilename(e.target.files[0].name);
  };

  const refreshStatus = () => {
    setScanningCompleted(true);
  }

  const onSubmit = async e => {
    e.preventDefault();
    
    const formData = new FormData();
    formData.append('file', file);

    const value = file.name;
    const ext = value.substr(value.lastIndexOf("."));
    if (ext === '.zip' || ext === '.jar') {
      console.log('zip file upload!');
      setIsUpload(false);
    } else {
      confirmAlert({
        title: 'Sorry invalid extention!',
        message: 'Allowed extensions are: .zip and .jar. Upload another file?',
        buttons: [
          {
            label: 'OK',
            // onClick: () => alert('OK Clicked'),
            className: classes.confirmPopupOkBtn
          }
        ]
      })
    }

    // try {
    //   const res = await axios.post('/upload', formData, {
    //     headers: {
    //       'Content-Type': 'multipart/form-data'
    //     },
    //     onUploadProgress: progressEvent => {
    //       setUploadPercentage(
    //         parseInt(
    //           Math.round((progressEvent.loaded * 100) / progressEvent.total)
    //         )
    //       );
    //     }
    //   });

    //   // Clear percentage
    //   setTimeout(() => setUploadPercentage(0), 10000);

    //   const { fileName, filePath } = res.data;

    //   setUploadedFile({ fileName, filePath });

    //   console.log('File Uploaded');
    // //   setMessage('File Uploaded');
    // } catch (err) {
    //   if (err.response.status === 500) {
    //       console.log('There was a problem with the server');
    //     // setMessage('There was a problem with the server');
    //   } else {
    //       console.log(err.response.data.msg);
    //     // setMessage(err.response.data.msg);
    //   }
    //   setUploadPercentage(0)
    // }
  };

  return (
    <Fragment>
      {/* {message ? <Message msg={message} /> : null} */}
      {(() => {
        if (isUploadPage) {
          return (
           <UploadPageNew/>
          )
        } else if (!isUploadPage) {
          return (
            <center>

              {(() => {
                if (!isScanningCompleted) {
                  return (
                    <img src={load6} alt="loading..." className={classes.imageClass} />
                  )
                } else if (isScanningCompleted) {
                  return (
                    <img src={tick7} className={classes.imageClass} />
                  )
                }
              })()}

              <div>
                <p>{isScanningCompleted ? 'Scanning Completed' : 'Scanning In Progress'}</p>
                <p>{isScanningCompleted ? 'All' : '124 out of 180'} files have been scanned</p>
              </div>
              <div>
                <p>Number of services found : <b>3</b> </p>
                <p>Number of classes found : <b>11</b> </p>
                <p>Number of interfaces found : <b>5</b> </p>
              </div>
              {(() => {
                if (!isScanningCompleted) {
                  return (
                    <Button className={classes.submitBtnColor} onClick={refreshStatus}>Refresh Status</Button>
                  )
                } else if (isScanningCompleted) {
                  return (
                    <Button className={classes.submitBtnColor} component={Link} to="/dashboard" onClick={() => setActiveLink('Dashboards')}>View Analysis Report</Button>
                  )
                }
              })()}
            </center>
          )
        }
      })()}

    </Fragment>
  );
};

export default FileUpload;


import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2, 4),
      },
      card: {
        height: '100%',
        maxWidth: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        boxShadow: '5px 5px 10px thistle',
        borderRadius: '20px',
        textAlign: 'center',
      },
      cardLeft: {
        alignItems: 'left',
        textAlign: 'left'
      },
      icon: {
        padding: theme.spacing(2, 0),
      },
      title: {
        padding: theme.spacing(2),
        fontWeight: 'bold',
        color: "purple"
      },
      featureList: {
        padding: theme.spacing(.5),
        color: '#000000',

      },
      featureHeaders: {
        padding: theme.spacing(.5),
        color: '#808080',
        fontWeight: '400'
      },
      bottomCard: {
        
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        boxShadow: '5px 5px 10px thistle',
        borderRadius: '25px',
        textAlign: 'center',
        paddingBottom: "20px"
      }
}
)
)
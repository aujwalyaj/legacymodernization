import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import useStyles from './styles.js';
import { Card, CardContent } from "@material-ui/core";

export default function OutlinedCard() {
  const classes = useStyles();

  const getApiName = (str) => {
    const strArray = str.split("/");
    return (strArray[strArray.length - 1])
  }

  const apis = [
        {
          "apiPath": "/api/v1/order/submitOrder",
          "apiClass": "partner2021/src/main/java/com/accenture/controller/OrderController.java"
        },{
            "apiPath": "/api/v1/partner/getPartnerDetails",
            "apiClass": "partner2021/src/main/java/com/accenture/controller/PartnerController.java"
        }
    ]

  return (
    <Container component="section" maxWidth="lg" className={classes.root}>
        <Typography variant="h4" className={classes.title}>
                APIs used in your Code
        </Typography>
        <Grid container  spacing={4} pt={25}>
                    {
                        apis.map((api=>{ return (
                            <Grid item key={api._id} xs={12} sm={6} >
                                <Card className={classes.card}>
                                    <Typography variant="h6" className={classes.title}>
                                       {getApiName(api.apiPath)}
                                    </Typography>
                                    <CardContent className={classes.cardLeft}>
                                      <Typography  variant="body2">
                                          <span className={classes.featureHeaders}>API Path:</span>
                                          <span className={classes.featureList}>{api.apiPath}</span>
                                      </Typography>
                                      <Typography  variant="body2">
                                          <span className={classes.featureHeaders}>Class Name:</span>
                                          <span className={classes.featureList}>{getApiName(api.apiClass)}</span>
                                      </Typography>
                                      <Typography variant="body2">
                                          <span className={classes.featureHeaders}>Class Path:</span>
                                          <span className={classes.featureList}>{api.apiClass}</span>
                                      </Typography>
                                      
                                    </CardContent>
                                </Card>
                            </Grid>
                        )
                    }))}
        </Grid>

    </Container>
  );
}

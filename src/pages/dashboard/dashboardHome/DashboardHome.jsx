import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import useStyles from './styles.js';
import { ResponsiveContainer, BarChart, XAxis, YAxis, Tooltip, Legend, Bar} from 'recharts'
import { Link } from "react-router-dom";

const data = [
    {
      name: 'Service A',
      classes: 4000,
      interfaces: 2400
    },
    {
      name: 'Service B',
      classes: 3000,
      interfaces: 1398
    },
    {
      name: 'Service C',
      classes: 2000,
      interfaces: 2000
    },
    {
      name: 'Service D',
      classes: 2780,
      interfaces: 3908
    },
    {
      name: 'Service E',
      classes: 1890,
      interfaces: 4800
    }
  ];

export default function OutlinedCard({setActiveLink}) {
  const classes = useStyles();

  return (
    <Container component="section" maxWidth="lg" className={classes.root}>
      <Grid container spacing={3} alignItems="stretch">
        <Grid item xs={12} sm={4} onClick={()=> setActiveLink("APIs")}> 
          <div className={classes.card} >
            <Typography variant="h6" component="h3" className={classes.title} component={Link} to = "/dashboard/apis">
              APIs
            </Typography>
            <Typography variant="h4">
              2
            </Typography>
            <Typography className={classes.featureList} variant="body2">
              Overall number of Services
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} sm={4}>
          <div className={classes.card}>
            <Typography variant="h6" component="h3" className={classes.title}>
              Classes
            </Typography>
            <Typography variant="h4">
              21
            </Typography>
            <Typography className={classes.featureList} variant="body2">
            Overall number of Classes
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} sm={4}>
          <div className={classes.card}>
            <Typography variant="h6" component="h3" className={classes.title}>
            Interfaces
            </Typography>
            <Typography variant="h4">
              11
            </Typography>
            <Typography className={classes.featureList} variant="body2">
            Overall number of Interfaces
            </Typography>
          </div>
        </Grid>
      </Grid>
      <br/>
      <br/>
      <br/>
      <Grid container spacing={3} alignItems="stretch">
        <Grid item lg={12}>
          <div className={classes.bottomCard}>
            <Typography variant="h6" component="h3" className={classes.title}>
              Service Analytics (Bar Chart)
            </Typography>
            {/* <Typography variant="h4">
              
            </Typography>
            <Typography className={classes.featureList}>
              
            </Typography> */}
             
            <ResponsiveContainer width="100%" aspect={4/1}>
                <BarChart data={data}>
                {/* <CartesianGrid strokeDasharray="3 3" /> */}
                    <XAxis dataKey="name" stroke="purple"/>
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar name="Classes" dataKey="classes" stackId="a" fill="thistle"  />
                <Bar name="Interfaces" dataKey="interfaces" fill="#82ca9d" />
                </BarChart>
            </ResponsiveContainer>
        
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}


import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
    root: {
        padding: theme.spacing(6, 4),
      },
      card: {
        height: '100%',
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        boxShadow: '5px 5px 10px thistle',
        borderRadius: '20px',
        textAlign: 'center'
      },
      icon: {
        padding: theme.spacing(2, 0),
      },
      title: {
        padding: theme.spacing(2),
        fontWeight: 'bold',
        color: "purple",
        textDecoration: 'none'
      },
      featureList: {
        padding: theme.spacing(2),
        color: '#808080',

      },
      bottomCard: {
        
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        boxShadow: '5px 5px 10px thistle',
        borderRadius: '25px',
        textAlign: 'center',
        paddingBottom: "20px"
      }
}
)
)
import React, {useState} from "react";
import { Drawer, Typography, Container, CssBaseline } from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Navbar from "../navbar/Navbar";
import useStyles from './styles'
import Sidebar from "../sidebar/Sidebar";
import DashboardHome from "../../pages/dashboard/dashboardHome/DashboardHome"
import FileUpload from "../../pages/uploadPage/upload.jsx";
import Popup from "../popup/Popup"
import UserGuide from "../../pages/userGuidePage/userGuide";
import Login from "../../pages/loginPage/Login";
import UploadPageNew from "../../pages/uploadPage/upload/UploadPageNew"
import LandingPage from "../../pages/home/LandingPage";
import APIs from "../../pages/dashboard/apis/APIs";
import About from "../../pages/aboutPage/About";
import OutlinedCard from "../../pages/dashboard/dashboardHome/DashboardHome";



function App() {
  const classes = useStyles();

  const [sideNavOpen, setSideNavOpen] = useState(false)
  const [activeLinkValue, setactiveLinkValue] = useState("Home")
  const [openPopup, setOpenPopup]= useState(false)

  function setSideNav(signIn) {
    if(signIn) {
      setSideNavOpen(false);
    } else {
      setSideNavOpen(!sideNavOpen)
    }
    
  }

  function setActiveLink (activeLink) {
    setactiveLinkValue(activeLink)
  }

  return (
    <Router>
      <Navbar onClickOpen={setSideNav} setActiveLink={setActiveLink} setOpenPopup={setOpenPopup}/>
      <CssBaseline />
      <Drawer
        variant="persistent"
        anchor="left"
        open={sideNavOpen}
        classes={{ paper: classes.drawerPaper }}
      >
        <Sidebar activeLinkValue={activeLinkValue}/>

      </Drawer>
      <div className={sideNavOpen ? classes.routeContent : classes.routeContentHome}>
        <Switch>
          <Route exact path="/">
            {/* <Home setActiveLink={setActiveLink}/> */}
            <LandingPage setActiveLink={setActiveLink}/>
          </Route>

          <Route exact path="/dashboard">
            <OutlinedCard setActiveLink={setActiveLink}/>
          </Route>

          <Route exact path = "/dashboard/apis">
            <APIs/>
          </Route>

          <Route exact path="/upload">
            <FileUpload setActiveLink={setActiveLink}/>
          </Route>

          <Route exact path="/uploadNew">
            <UploadPageNew/>
          </Route>

          <Route exact path="/userGuide">
            <UserGuide/>
          </Route>

          <Route exact path="/login">
            <Login setActiveLink={setActiveLink}/>
          </Route>
          
          <Route exact path="/about">
            <Container>
              <About/>
            </Container>
          </Route>

        </Switch>
      </div>
      <Popup 
        title='Sign In'
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}>
          <Login/>
      </Popup>
    </Router>
    
  )
}

export default App
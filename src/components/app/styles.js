import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
  drawerPaper: {
    // width: 'inherit' 
    width: '240px',
    marginTop: '65px',
    backgroundColor: '#EFE8F3'
  },
  routeContent: {
    marginTop: '65px',
    marginLeft: '240px',
    transitionDuration: '0.5s'
  },
  routeContentHome: {
    marginTop: '65px',
    marginLeft: '0px',
    transitionDuration: '0.5s'
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  }

}
)
)
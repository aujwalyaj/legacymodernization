import React, {useState} from 'react'
import { AppBar, Toolbar, IconButton, MenuItem, Menu, Typography } from '@material-ui/core'
import { Dehaze, GetApp } from '@material-ui/icons'
import { Link, useLocation } from 'react-router-dom';
import useStyles from './styles'

const Navbar = ({onClickOpen, setActiveLink, setOpenPopup}) => {
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
    const classes = useStyles();
    const location = useLocation();
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const handleMobileMenuClose = () => setMobileMoreAnchorEl(null);
    const mobileMenuId = 'primary-search-account-menu-mobile';

    const renderMobileMenu = (
        <Menu anchorEl={mobileMoreAnchorEl} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} id={mobileMenuId} keepMounted transformOrigin={{ vertical: 'top', horizontal: 'right' }} open={isMobileMenuOpen} onClose={handleMobileMenuClose}>
          <MenuItem>
            <IconButton component={Link} to="/" aria-label="Download Report" color="inherit" >        
                <GetApp className={classes.downloadIcon}/>
            </IconButton>
            <p>Report</p>
          </MenuItem>
        </Menu>
      );

    return (
        <>
      <AppBar position="fixed" className={classes.appBar} color="inherit">
        <Toolbar>
          <Typography variant="h4" className={classes.title} color="inherit">
                    <Dehaze className={classes.dehaze} onClick={() => onClickOpen(false)}/>
                    <Typography variant="h4" onClick={()=> setActiveLink("Home")} className={classes.title} component={Link} to = "/">Legacy Platform Analyser</Typography>
          </Typography>
          <div className={classes.grow} />
          <div>
          {/* <Typography variant="h6" onClick={()=> setOpenPopup(true)} >Sign In</Typography> */}
            <Typography variant="h6" component={Link} to = "/login" onClick={() => onClickOpen(true)} className={classes.linkText} >Sign In</Typography>
          </div>
          {(location.pathname === '/dashboard') && (
          <div className={classes.button}>
            <IconButton component={Link} to="/cart"  aria-label="Download Report" color="inherit">
                <GetApp className={classes.downloadIcon}/>
            </IconButton>
          </div>
          )}
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </>
    )
}

export default Navbar

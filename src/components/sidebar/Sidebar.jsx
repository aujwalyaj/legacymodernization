import React, {useEffect} from 'react'
import { List, ListItemIcon, ListItem, ListItemText, Divider,Collapse } from '@material-ui/core'
import { Link } from 'react-router-dom'
import { Home, Info, ExpandLess, ExpandMore, LibraryBooks, Assessment, CloudUpload, Storage } from '@material-ui/icons';
import useStyles from './styles';


const Sidebar = ({activeLinkValue}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false)
    const [activeLink, setActiveLink] = React.useState(activeLinkValue)
    
    function handleClick() {
        setOpen(!open)
    }

    function handleActiveClick(activeText) {
        setActiveLink(activeText)
    }

    useEffect(()=> {
        handleActiveClick(activeLinkValue);
    }, [activeLinkValue]);

    return (
        <>
        <List>
          <Link to="/" className={classes.link}>
            <ListItem button className={activeLink === 'Home' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Home')}>
              <ListItemIcon>
                <Home className={classes.sideNavIcon}/>
              </ListItemIcon>
              <ListItemText primary={"Home"} className={classes.sideNavText}/>
            </ListItem>
          </Link>

          <Link to="/upload" className={classes.link}>
                <ListItem button className={activeLink === 'Upload Your Code' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Upload Your Code')}>
                <ListItemIcon>
                    <CloudUpload className={classes.sideNavIcon}/>
                </ListItemIcon>
                <ListItemText primary={"Upload Your Code"} className={classes.sideNavText}/>
                </ListItem>
            </Link>

            <Link to="/upload" className={classes.link}>
                <ListItem button className={activeLink === 'Refactor Your Database' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Refactor Your Database')}>
                <ListItemIcon>
                    <Storage className={classes.sideNavIcon}/>
                </ListItemIcon>
                <ListItemText primary={"Refactor Your DB"} className={classes.sideNavText}/>
                </ListItem>
            </Link>

            <Link to="/dashboard" className={classes.link}>
            <ListItem button  className={activeLink === 'Dashboards' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Dashboards')} >
                <ListItemIcon className={classes.menuItemIcon}>
                <Assessment className={classes.sideNavIcon}/>
                </ListItemIcon>
                <ListItemText primary="Dashboards" className={classes.sideNavText}/>
                {open ? <ExpandLess onClick={handleClick}/> : <ExpandMore onClick={handleClick}/>}
            </ListItem>
            </Link>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <Divider />
                <List component="div" disablePadding>
                <Link to="/dashboard/apis" className={classes.link}>
                    <ListItem button className={activeLink === 'APIs' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('APIs')} >
                        <ListItemText inset primary="APIs" className={classes.sideNavText}/>
                    </ListItem>
                </Link>
                
                <ListItem button className={activeLink === 'Classes' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Classes')}>
                    <ListItemText inset primary="Classes" className={classes.sideNavText}/>
                </ListItem>
                <ListItem button className={activeLink === 'Interfaces' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('Interfaces')}>
                    <ListItemText inset primary="Interfaces" className={classes.sideNavText}/>
                </ListItem>
                </List>
            </Collapse>

           

            <Link to="/userGuide" className={classes.link}>
                <ListItem button className={activeLink === 'User Guide' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('User Guide')}>
                <ListItemIcon>
                    <LibraryBooks className={classes.sideNavIcon}/>
                </ListItemIcon>
                <ListItemText primary={"User Guide"} className={classes.sideNavText}/>
                </ListItem>
            </Link>

            <Link to="/about" className={classes.link}>
                <ListItem button className={activeLink === 'About' ? classes.activeLink : classes.menuItem} onClick={() => handleActiveClick('About')}>
                <ListItemIcon>
                    <Info className={classes.sideNavIcon}/>
                </ListItemIcon>
                <ListItemText primary={"About"} className={classes.sideNavText}/>
                </ListItem>
            </Link>

         
        </List>
        </>
        
    )
}

export default Sidebar

import { makeStyles } from '@material-ui/core/styles';


export default makeStyles((theme) => ({
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  sideNavIcon: {
    color: '#800080'
  },
  menuItem: {
    '&:hover': {
      backgroundColor: 'thistle' 
    },
    // backgroundColor: theme.palette.action.selected
  },
  activeLink: {
    backgroundColor: 'thistle'
  },
  sideNavText: {
    fontWeight: '900',
    color: 'purple'
  },
  submitBtnColor: {
    backgroundColor: '#800080 !important',
    borderColor: '#800080 !important',
    color: 'white',
    borderRadius: '5px',
    height: '40px',
    border: 'none'
  },
  body: { overflow: 'auto' }

}
)
)
import { Dialog, DialogContent, DialogTitle, Typography, Button } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React from 'react'
import useStyles from './styles'

const Popup = (props) => {
    const classes = useStyles();
    const {title, children, openPopup, setOpenPopup} = props;
    return (
        <Dialog open={openPopup} maxWidth="md" classes={{paper : classes.dialogWrapper}} >
            <DialogTitle>
                <div className={classes.header}>
                    <Typography variant="h5" className={classes.title}>
                        {title}
                    </Typography>
                    <Button color="secondary" onClick={()=> setOpenPopup(false)}><Close/></Button>
                </div>
            </DialogTitle>
            <DialogContent dividers >
                {children}
            </DialogContent>
        </Dialog>
    )
}

export default Popup

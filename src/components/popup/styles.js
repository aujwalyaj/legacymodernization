import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    dialogWrapper: {
        padding:theme.spacing(2),
        position: 'absolute',
        //top: theme.spacing(5)
    },
    header: {
        display:'flex',
    },
    title : {
        flexGrow: '1',
    }

}
)
)
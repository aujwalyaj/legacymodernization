import { combineReducers } from 'redux';
const UPDATE_ACTIVE_LINK = 'UPDATE_ACTIVE_LINK';

export function updateActiveLink(link) {
    return {
      type: UPDATE_ACTIVE_LINK,
      link,
    }
}

const defaultLink = 'Home'

function activeLink (state= defaultLink, action) {
    switch (action.type) {
        case UPDATE_ACTIVE_LINK:
          return action.link
        default:
          return state;
    }
}

const activeLinkApp = combineReducers({
    activeLink
});
  
  export default activeLinkApp;